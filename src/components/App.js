import React, { Component } from 'react'
import Button from './Button'
import { sortBy } from 'lodash'
import classNames from 'classnames'

const SORTS = {
	NONE: user_list => user_list,
	NAME: user_list => sortBy(user_list, 'name'),
	EMAIL: user_list => sortBy(user_list, 'email')
}

class App extends Component {
	constructor(props) {
		super(props)

		this.state = {
			name: 'Darwin',
			fav_foods: ['pizza', 'hamburger', 'menudo', 'sinigang'],
			user_list: [],
			searchTerm: '',
			todos: [],
			isLoading: true,
			sortKey: 'NONE'
		}

		this.onDismiss = this.onDismiss.bind(this)
		this.onSearchChange = this.onSearchChange.bind(this)
		this.setTodos = this.setTodos.bind(this)
		this.onSort = this.onSort.bind(this)
	}

	onSort(sortKey) {
		this.setState({ sortKey })
	}

	setTodos(todos) {
		this.setState({todos: todos})
	}

	componentDidMount() {
		fetch('http://jsonplaceholder.typicode.com/todos')
			.then(data => data.json())
			.then(todos => this.setTodos(todos))
			.catch(error => console.log(error))

		fetch('http://jsonplaceholder.typicode.com/users/')
			.then(data => data.json())
			.then(users => this.setState({
				user_list: users,
				isLoading: false
			}))
			.catch(error => console.log(`ERROR fetch: ${error}`))
	}

	onDismiss(id) {
		const updatedUser = this.state.user_list.filter(user => user.id !== id)
		this.setState({
			user_list: [...updatedUser]
		})
	}

	onSearchChange(event) {
		this.setState({
			searchTerm: event.target.value
		})
	}

	render() {
		console.log(this.state)
		const { fav_foods,
						user_list,
						searchTerm,
						todos,
						isLoading,
						sortKey
					} = this.state

		const foods = fav_foods.map(food => <li key={food}> {food} </li>)

		const displayTodo = todos.map(todo => {
			return <li key={todo.id}> {todo.title} </li>
		})

		// const users = user_list.map(user => {
		// 	return <li key={user.id}> {user.name} <button onClick={() => this.onDismiss(user.id)} type="button">Dismiss</button> </li>
		// })
		
		// Check if the value is object
		const checkIfObject = (user_list) => {
			return JSON.stringify(user_list) != "{}" ? true : false
		}

		return (
			<div>
				<Search value={searchTerm} onChange={this.onSearchChange}>
					Search
				</Search>

				{ 
					checkIfObject(user_list) &&
					<div>
					<Table 	list={user_list}
									pattern={searchTerm}
									onDismiss={this.onDismiss}
					/>

					<h1>SORTING TABLE</h1>
					<Table2 	list={user_list}
									pattern={searchTerm}
									onDismiss={this.onDismiss}
									sortKey={sortKey}
									onSort={this.onSort}
					/>
					</div>
				}

				{
					isLoading && <Loading />
				}
			</div>
		)
	}
}

// class Search extends Component {
// 	componentDidMount() {
// 		if (this.input) {
// 			this.input.focus()
// 		}
// 	}

// 	render() {
// 		const { value, onChange, children } = this.props
// 		return (
// 			<form>
// 				{ children } 
// 				<input 	type="text"
// 								value={value}
// 								onChange={onChange} 
// 								ref={(node) => {this.input = node}}
// 				/>
// 			</form>
// 		)
// 	}
// }

const Search = ({value, onChange, children}) => {
	return (
		<form>
				{ children }
				<input 	type="text"
								value={value}
								onChange={onChange}
				/>
		</form>
	)
}

class Table extends Component {
	render() {
		const { list,
						pattern,
						onDismiss
					} = this.props

		// const isSearched = searchTerm => user => user.name.toLowerCase().includes(searchTerm.toLowerCase())

		const isSearched = function(searchTerm) {
			return function(user) {
				return user.name.toLowerCase().includes(searchTerm.toLowerCase())
			}
		}

		const search = list.filter(isSearched(pattern)).map(
			user => <li key={user.id}> {user.name} 
							<Button onClick={() => onDismiss(user.id)}>Dismiss</Button>
							</li>
		)

		return (
			<div>
				{ search }
			</div>
		)
	}
}

class Table2 extends Component {
	render() {
		const { list,
						pattern,
						onDismiss,
						sortKey,
						onSort
					} = this.props

		// const isSearched = searchTerm => user => user.name.toLowerCase().includes(searchTerm.toLowerCase())

		const isSearched = function(searchTerm) {
			return function(user) {
				return user.name.toLowerCase().includes(searchTerm.toLowerCase())
			}
		}

		const search = SORTS[sortKey](list).map(
			user => <li key={user.id}> <b>{user.name}</b> Email: {user.email}
							<Button onClick={() => onDismiss(user.id)}>Dismiss</Button>
							</li>
		)

		return (
			<div>
				<Sort sortKey={'NAME'} onSort={onSort} activeSortKey={sortKey}>
					Name
				</Sort>
				<Sort sortKey={'EMAIL'} onSort={onSort} activeSortKey={sortKey}>
					Email
				</Sort>
				{ search }
			</div>
		)
	}
}

const Sort = ({ sortKey, onSort, children, activeSortKey }) => {
	// const sortClass = ['button-inline']
	// if (sortKey === activeSortKey) {
	// 	sortClass.push('button-active')
	// }
	
	const sortClass = classNames(
		'button-inline',
		{ 'button-active': sortKey === activeSortKey }
	)
	return (
		<Button onClick={() => onSort(sortKey)} className={sortClass}>
			{ children }
		</Button>
	)
}

const Loading = () => <div>Loading...</div>

const withFoo = Component => props => <Component { ...props } />

export default App