import React, { Component } from 'react'
import PropTypes from 'prop-types'

const Button = (props) => {
	const { onClick, className, children } = props
	return (
		<button 
			onClick={onClick}
			className={className}
			type="button">
			{ children }
		</button>
	)
}

Button.propTypes = {
	onClick: PropTypes.func.isRequired,
	className: PropTypes.string,
	children: PropTypes.node.isRequired,
}

Button.defaultProps = {
	className: '',
}

// The basic PropTypes for primitives and complex objects are
// • PropTypes.array
// • PropTypes.bool
// • PropTypes.func
// • PropTypes.number
// • PropTypes.object

// • PropTypes.node
// • PropTypes.elemen
//• PropTypes.string

// class Button extends Component {
// 	render() {
// 		const { onClick, className, children } = this.props
// 		return (
// 			<button 
// 				onClick={onClick}
// 				className={className}
// 				type="button">
// 				{ children }
// 			</button>
// 		)
// 	}
// }

export default Button