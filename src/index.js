import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'

import css from './css/styles.css'
// import 'bootstrap'

ReactDOM.render(<App />, document.getElementById('app'))